from xmlrpc.server import SimpleXMLRPCServer
    
import re

def isCPFValid(CPF):

    if not isinstance(CPF,str):
        return False

    CPF = re.sub("[^0-9]",'',CPF)

    if CPF=='00000000000' or CPF=='11111111111' or CPF=='22222222222' or CPF=='33333333333' or CPF=='44444444444' or CPF=='55555555555' or CPF=='66666666666' or CPF=='77777777777' or CPF=='88888888888' or CPF=='99999999999':
        return False

    if len(CPF) != 11:
        return False

    sum = 0
    weight = 10

    for n in range(9):
        sum = sum + int(CPF[n]) * weight

        weight = weight - 1

    verifyingDigit = 11 -  sum % 11

    if verifyingDigit > 9 :
        firstVerifyingDigit = 0
    else:
        firstVerifyingDigit = verifyingDigit

    sum = 0
    weight = 11
    for n in range(10):
        sum = sum + int(CPF[n]) * weight

        weight = weight - 1

    verifyingDigit = 11 -  sum % 11

    if verifyingDigit > 9 :
        secondVerifyingDigit = 0
    else:
        secondVerifyingDigit = verifyingDigit

    if CPF[-2:] == "%s%s" % (firstVerifyingDigit,secondVerifyingDigit):
        return True
    return False

def is_par(n):

    return n % 2 == 0

server = SimpleXMLRPCServer(("localhost", 8000))
print("Listening on port 8000...") 
server.register_function(isCPFValid, "is_CPFValid")
server.register_function(is_par, "is_NumeroPar")
server.serve_forever()
