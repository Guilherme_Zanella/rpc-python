import xmlrpc.client

with xmlrpc.client.ServerProxy("http://localhost:8000/") as proxy:

    OP = input("1 - Validar CPF ou 2 - Verificar par ou ímpar:")

    if(OP == "1"):
        CPF = input("Digite o CPF: (111.111.111-11): ")
    
        print(proxy.is_CPFValid(CPF))

    elif(OP == "2"):
        print("3 é par: %s" % str(proxy.is_NumeroPar(3)))
        print("100 é par: %s" % str(proxy.is_NumeroPar(100)))

    else:
        print("Opção inválida")


    fechar = input("Pressione ENTER para sair")  
